%{?mingw_package_header}

Name:           mingw-spice-gtk
Version:        0.38
Release:        5%{?dist}
Summary:        A GTK+ widget for SPICE clients

License:        LGPLv2+
URL:            http://spice-space.org/page/Spice-Gtk
Source0:        https://www.spice-space.org/download/gtk/spice-gtk-%{version}%{?_version_suffix}.tar.xz
Source1:        https://www.spice-space.org/download/gtk/spice-gtk-%{version}%{?_version_suffix}.tar.xz.sig
Source2:        victortoso-E37A484F.keyring
Patch0001:      0001-channel-main-Avoid-macro-side-effects.patch
Patch0002:      0002-channel-main-Check-proper-size-and-caps-handling-VD_.patch
Patch0003:      0003-build-sys-bump-polkit-requirement-to-0.101.patch
Patch0004:      0004-spice-channel-Read-all-available-data-from-SASL-buff.patch
Patch0005:      0005-meson-add-wayland-protocols.patch
Patch0006:      0006-wayland-add-wayland-extensions-functions.patch
Patch0007:      0007-spice-gtk-save-mouse-button-state-on-mouse-click.patch
Patch0008:      0008-wayland-fix-mouse-lock-in-server-mode.patch
Patch0009:      0009-channel-main-Handle-some-detailed-error-for-VD_AGENT.patch
Patch0010:      0010-quic-Check-we-have-some-data-to-start-decoding-quic-.patch
Patch0011:      0011-quic-Check-image-size-in-quic_decode_begin.patch
Patch0012:      0012-quic-Check-RLE-lengths.patch
Patch0013:      0013-quic-Avoid-possible-buffer-overflow-in-find_bucket.patch

# covscan fixes
Patch0014:      0014-Remove-some-warnings-from-Clang-static-analyzer.patch
Patch0015:      0015-ssl_verify-Do-not-check-IP-if-we-fail-to-resolve-it.patch

# usbredir-redirect-on-connect
Patch0016:      0016-usb-backend-Fix-spice-usbredir-redirect-on-connect-o.patch

# more covscan fixes
Patch0017:      0017-empty_cd_clicked_cb-g_free-basename.patch
Patch0018:      0018-spice_usbutil_parse_usbids-verify-at-least-one-vendo.patch
Patch0019:      0019-sink_event_probe-do-not-keep-duration-in-a-variable.patch
Patch0020:      0020-mark_false_event_id-is-guint-assign-0-to-it-not-FALS.patch
Patch0021:      0021-usb-backend-create_emulated_device-assert-address-32.patch
Patch0022:      0022-spice-utils-allocate-ctx-after-g_return_val_if_fail.patch

# migration fixes: some earlier patches to make the following patches apply
Patch0023:      0023-channel-main-Fix-indentation.patch
Patch0024:      0024-channel-main-Fix-indentation.patch
Patch0025:      0025-channel-main-Remove-unused-declaration.patch
# related to patch 0009
Patch0026:      0026-main-add-a-few-missing-vdagent-capability-descriptio.patch
# same file, safer code
Patch0027:      0027-main-add-stricter-pre-condition-on-display-id-value.patch
# migration fixes: the patches
Patch0028:      0028-channel-main-Use-heap-and-reference-counting-for-spi.patch
Patch0029:      0029-channel-main-Copy-SpiceMigrationDstInfo-into-spice_m.patch
Patch0030:      0030-channel-main-Make-more-clear-that-host_data-and-cert.patch
Patch0031:      0031-channel-main-Handle-not-terminated-host_data-and-cer.patch

# The following patches are not upstream
# Enable build with no SpiceQmpPort (and build without)
Patch1001:      1001-build-Make-build-of-SpiceQmpPort-optional.patch
# Support older version of libjpeg-turbo
Patch1002:      1002-MINGW-meson.build-move-libjpeg-from-pkgconfig-to-hea.patch


BuildArch: noarch
ExclusiveArch:  %{ix86} x86_64

BuildRequires: meson
BuildRequires: git-core
BuildRequires: mingw32-filesystem >= 95
BuildRequires: mingw64-filesystem >= 95
BuildRequires: mingw32-gcc
BuildRequires: mingw64-gcc
BuildRequires: mingw32-binutils
BuildRequires: mingw64-binutils
BuildRequires: glib2-devel

BuildRequires: mingw32-gtk3 >= 2.91.3
BuildRequires: mingw64-gtk3 >= 2.91.3
BuildRequires: mingw32-pixman
BuildRequires: mingw64-pixman
BuildRequires: mingw32-openssl
BuildRequires: mingw64-openssl
BuildRequires: mingw32-libjpeg-turbo
BuildRequires: mingw64-libjpeg-turbo
BuildRequires: mingw32-zlib
BuildRequires: mingw64-zlib
BuildRequires: mingw32-gstreamer1
BuildRequires: mingw64-gstreamer1
BuildRequires: mingw32-gstreamer1-plugins-base
BuildRequires: mingw64-gstreamer1-plugins-base
BuildRequires: mingw32-opus
BuildRequires: mingw64-opus
BuildRequires: mingw32-spice-protocol >= 0.12.14
BuildRequires: mingw64-spice-protocol >= 0.12.14
BuildRequires: mingw32-libusbx >= 1.0.21
BuildRequires: mingw64-libusbx >= 1.0.21
BuildRequires: mingw32-usbredir
BuildRequires: mingw64-usbredir
BuildRequires: python3-devel
BuildRequires: python3-six
BuildRequires: python3-pyparsing

# Hack because of bz #613466
BuildRequires: intltool
BuildRequires: libtool

%description
Client libraries for SPICE desktop servers.


# Mingw32
%package -n mingw32-spice-gtk3
Summary: %{summary}
Requires: mingw32-spice-glib = %{version}-%{release}
Requires: mingw32-gtk3
Requires: pkgconfig
Obsoletes: mingw32-spice-gtk < 0.32
Obsoletes: mingw32-spice-gtk-static < 0.32-2

%description -n mingw32-spice-gtk3
Gtk+3 client libraries for SPICE desktop servers.

%package -n mingw32-spice-glib
Summary: GLib-based library to connect to SPICE servers
Requires: pkgconfig
Requires: mingw32-glib2
Requires: mingw32-spice-protocol
Conflicts: mingw64-spice-glib <= 0.35

%description -n mingw32-spice-glib
A SPICE client library using GLib2.

# Mingw64
%package -n mingw64-spice-gtk3
Summary: %{summary}
Requires: mingw64-spice-glib = %{version}-%{release}
Requires: mingw64-gtk3
Requires: pkgconfig
Obsoletes: mingw64-spice-gtk < 0.32
Obsoletes: mingw64-spice-gtk-static < 0.32-2

%description -n mingw64-spice-gtk3
Gtk+3 client libraries for SPICE desktop servers.

%package -n mingw64-spice-glib
Summary: GLib-based library to connect to SPICE servers
Requires: pkgconfig
Requires: mingw64-glib2
Requires: mingw64-spice-protocol
Conflicts: mingw32-spice-glib <= 0.35

%description -n mingw64-spice-glib
A SPICE client library using GLib2.

%{?mingw_debug_package}


%prep
%autosetup -S git_am -n spice-gtk-%{version}%{?_version_suffix}

%build
%mingw_meson \
    -Dcelt051=disabled \
    -Dusbredir=enabled \
    -Dsasl=disabled \
    -Dcoroutine=winfiber \
    -Dsmartcard=disabled \
    -Dgtk_doc=disabled

%mingw_ninja


%install
rm -rf $RPM_BUILD_ROOT
# FIXME: Upgrade mingw-fileysystem package with version that provides the
# %mingw_ninja_install macro in macros.mingw file. The following code has been
# taken from that file.
#
# https://src.fedoraproject.org/rpms/mingw-filesystem/c/ff5a0b892ec7fad2b158ad1838f7d78c8e093e1c
%if 0%{?mingw_build_win32} == 1
DESTDIR=%{buildroot} ninja -C build_win32$MINGW_BUILDDIR_SUFFIX install
%endif
%if 0%{?mingw_build_win64} == 1
DESTDIR=%{buildroot} ninja -C build_win64$MINGW_BUILDDIR_SUFFIX install
%endif

# Libtool files don't need to be bundled
find $RPM_BUILD_ROOT -name "*.la" -delete
# man pages don't need to be bundled
find $RPM_BUILD_ROOT -name "*.1" -delete

%mingw_find_lang spice-gtk --all-name

# Mingw32
%files -n mingw32-spice-glib -f mingw32-spice-gtk.lang
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc README.md
%doc CHANGELOG.md
%{mingw32_bindir}/libspice-client-glib-2.0-8.dll
%{mingw32_bindir}/spicy-screenshot.exe
%{mingw32_bindir}/spicy-stats.exe
%{mingw32_libdir}/libspice-client-glib-2.0.dll.a
%{mingw32_libdir}/pkgconfig/spice-client-glib-2.0.pc
%{mingw32_includedir}/spice-client-glib-2.0

%files -n mingw32-spice-gtk3
%defattr(-,root,root)
%{mingw32_bindir}/libspice-client-gtk-3.0-5.dll
%{mingw32_bindir}/spicy.exe
%{mingw32_libdir}/libspice-client-gtk-3.0.dll.a
%{mingw32_libdir}/pkgconfig/spice-client-gtk-3.0.pc
%{mingw32_includedir}/spice-client-gtk-3.0

# Mingw64
%files -n mingw64-spice-glib -f mingw64-spice-gtk.lang
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc README.md
%doc CHANGELOG.md
%{mingw64_bindir}/libspice-client-glib-2.0-8.dll
%{mingw64_bindir}/spicy-screenshot.exe
%{mingw64_bindir}/spicy-stats.exe
%{mingw64_libdir}/libspice-client-glib-2.0.dll.a
%{mingw64_libdir}/pkgconfig/spice-client-glib-2.0.pc
%{mingw64_includedir}/spice-client-glib-2.0

%files -n mingw64-spice-gtk3
%defattr(-,root,root)
%{mingw64_bindir}/libspice-client-gtk-3.0-5.dll
%{mingw64_bindir}/spicy.exe
%{mingw64_libdir}/libspice-client-gtk-3.0.dll.a
%{mingw64_libdir}/pkgconfig/spice-client-gtk-3.0.pc
%{mingw64_includedir}/spice-client-gtk-3.0

%changelog
* Sun Feb 07 2021 Uri Lublin <uril@redhat.com> - 0.38-5
- Fix some migration issues
  Related: rhbz#1017261
- Fix some static analyzer issues
  Related: rhbz#1839104
- Fix spice-usbredir-redirect-on-connect
  Related: rhbz#1874740

* Thu Jun 11 2020 Uri Lublin <uril@redhat.com> - 0.38-4
- Rebuilt
  Related: rhbz#1841951

* Wed Jun 10 2020 Eduardo Lima (Etrunko) <etrunko@redhat.com> - 0.38-3
- Fix rpmdeplint result by adding Conflicts with older version of this package
  Related: rhbz#1841951

* Wed Jun 10 2020 Eduardo Lima (Etrunko) <etrunko@redhat.com> - 0.38-2
- Fix spec issue with packages installing conflicting files
  Related: rhbz#1841951

* Mon Jun 08 2020 Eduardo Lima (Etrunko) <etrunko@redhat.com> - 0.38-1
- Sync with spice-gtk package
  Resolves: rhbz#1841951

* Fri Aug 17 2018 Eduardo Lima (Etrunko) <etrunko@redhat.com> - 0.35-2
- ExclusiveArch: i686, x86_64
- BuildRequires: python3-devel
- BuildRequires: python3-six
- BuildRequires: python3-pyparsing
  Related: rhbz#1615874

* Fri Jul 13 2018 Victor Toso <victortoso@redhat.com> - 0.35-1
- Update to 0.35

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.34-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jul 31 2017 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.34-1
- new version

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.33-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.33-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Nov 15 2016 Victor Toso <victortoso@redhat.com> - 0.33-4
- Do not send unnecessary 0 bytes messages
  Resolves: https://bugs.freedesktop.org/show_bug.cgi?id=97227
- Fix hang over UsbDk failure and better error handling
  Resolves: https://bugs.freedesktop.org/show_bug.cgi?id=98686

* Tue Nov  8 2016 Victor Toso <victortoso@redhat.com> - 0.33-3
- Avoid crash on clipboard

* Mon Oct 31 2016 Victor Toso <victortoso@redhat.com> - 0.33-2
- Enable usbredir now with UsbDk integration in libusb

* Fri Oct 07 2016 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.33-1
- new version

* Tue Jun 28 2016 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.32-3
- Add missing Obsoletes for gtk2 and static packages

* Wed Jun 22 2016 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.32-2
- Remove static libraries

* Tue Jun 21 2016 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.32-1
- Update to spice-gtk 0.32 release

* Fri Mar 11 2016 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.31-1
- Update to spice-gtk 0.31 release

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.30-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Oct 07 2015 Christophe Fergeau <cfergeau@redhat.com> 0.30-1
- Update to spice-gtk 0.30

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.29-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Jun 15 2015 Marc-André Lureau <marcandre.lureau@redhat.com> 0.29-1
- Update to spice-gtk 0.29

* Wed Mar  4 2015 Marc-André Lureau <marcandre.lureau@redhat.com> 0.28-1
- Update to spice-gtk 0.28

* Tue Jan  6 2015 Fabiano Fidêncio <fidencio@redhat.com> 0.27.4
- Drop gstreamer-0.10 in favour to gstreamer-1.0

* Mon Dec 22 2014 Marc-André Lureau <marcandre.lureau@redhat.com> 0.27-3
- Fix usbredir crash on disconnection.

* Tue Dec 16 2014 Marc-André Lureau <marcandre.lureau@redhat.com> 0.27-2
- Fix authentication error handling regression.

* Thu Dec 11 2014 Marc-André Lureau <marcandre.lureau@redhat.com> 0.27-1
- Update to spice-gtk 0.27

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.23-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Feb 10 2014 Marc-André Lureau <marcandre.lureau@redhat.com> 0.23-1
- Update to spice-gtk 0.23

* Wed Dec 18 2013 Marc-André Lureau <marcandre.lureau@redhat.com> 0.22-1
- Update to spice-gtk 0.22

* Wed Sep 18 2013 Marc-André Lureau <marcandre.lureau@redhat.com> 0.21-1
- Update to spice-gtk 0.21

* Thu Aug 01 2013 Christophe Fergeau <cfergeau@redhat.com> 0.20-1
- Update to spice-gtk 0.20

* Sun Jun 16 2013 Erik van Pienbroek <epienbro@fedoraproject.org> - 0.19-3
- Rebuild to resolve InterlockedCompareExchange regression in mingw32 libraries

* Sat Jun 15 2013 Erik van Pienbroek <epienbro@fedoraproject.org> - 0.19-2
- Rebuild to resolve InterlockedCompareExchange regression in mingw32 libraries

* Thu Apr 11 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.19-1
- Update to spice-gtk 0.19

* Wed Feb 13 2013 Christophe Fergeau <cfergeau@redhat.com> - 0.18-1
- Update to spice-gtk 0.18

* Wed Feb  6 2013 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.17-1
- Update to spice-gtk 0.17

* Thu Jan 24 2013 Christophe Fergeau <cfergeau@redhat.com> - 0.16-2
- Add missing Requires (spice-protocol is required by
  spice-client-glib-2.0.pc)

* Tue Jan 22 2013 Christophe Fergeau <cfergeau@redhat.com> - 0.16-1
- Update to spice-gtk 0.16

* Fri Dec 21 2012 Christophe Fergeau <cfergeau@redhat.com> - 0.15-2
- Update to the 0.15 release

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.12-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Fri May 11 2012 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.12-2
- Remove spice-protocol dependency

* Wed May  2 2012 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.12-1
- Initial mingw64 packaging
